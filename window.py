from bearlibterminal import terminal as bearlib

import sys
import os

# Source: https://stackoverflow.com/a/44352931
def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    base_path = getattr(sys, '_MEIPASS', os.path.dirname(os.path.abspath(__file__)))
    return os.path.join(base_path, relative_path)


TITLE = "little sibling"
WINDOW_WIDTH = 21
WINDOW_HEIGHT = 17
MAP_WIDTH = 10
MAP_HEIGHT = 10
ENCODING = 'utf-8'
FONT = resource_path('./fonts/VeraMono.ttf')
FONT_SIZE = '20x20'
CELL_SIZE = '22x22'
FG_COLOR = 0xFF000000
BG_COLOR = 0xFFFFFFFF
# Option to draw UI rectangles
RECTANGLES = False

class Window:

    def __init__(self):
        self.height = WINDOW_HEIGHT
        self.width = WINDOW_WIDTH
        self.map_height = MAP_HEIGHT
        self.map_width = MAP_WIDTH
        self.gutter_width = self.width - self.map_width - 2
        self.rectangles = RECTANGLES
        self.dimensions = str(self.width) + 'x' + str(self.height)

        h, w = FONT_SIZE.split('x')
        self.font_height = int(h)
        self.font_width = int(w)

        self.encoding = ENCODING
        self.font = FONT
        self.font_size = FONT_SIZE
        self.title = TITLE
        self.cell_size = CELL_SIZE
        self.spacing = '1x1'
        self.fg_color = FG_COLOR
        self.bg_color = BG_COLOR
        self.configure()
        self.initialize()


    def configure(self):
        medium_size = '{}x{}'.format(self.font_width * 2, self.font_height * 2)
        medium_spacing = '{}x{}'.format(2, 2)
        large_size = '{}x{}'.format(self.font_width * 3 + self.font_width, self.font_height * 3 + self.font_height)
        large_spacing = '{}x{}'.format(3, 3)
        self.medium_str = '0xE000: {}, size={}, align=center, spacing={}; '.format(self.font, medium_size,
                                                                                    medium_spacing)
        self.large_str = '0xF000: {}, size={}, align=center, spacing={}; '.format(self.font, large_size, large_spacing)

        self.config_str = 'terminal: encoding={}; ' \
                          'font: {}, size={}, align=center, spacing={}; ' \
                          'window: size={} title={}, cellsize={}; ' \
                          'input: filter=[arrow, keypad, keyboard, system]'.format(self.encoding,
                                                                                   self.font,
                                                                                   self.font_size,
                                                                                   self.spacing,
                                                                                   self.dimensions,
                                                                                   self.title,
                                                                                   self.cell_size)

    def initialize(self):
        bearlib.open()
        bearlib.color(self.fg_color)
        bearlib.bkcolor(self.bg_color)
        bearlib.set(self.config_str)
        bearlib.set(self.medium_str)
        bearlib.set(self.large_str)

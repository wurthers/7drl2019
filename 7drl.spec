# -*- mode: python -*-

block_cipher = None


a = Analysis(['7drl.py'],
             pathex=['/home/wurthers/7drl2019'],
             binaries=[('shared/libBearLibTerminal.so', '.')],
             datas=[('fonts/VeraMono.ttf', './fonts')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='7drl',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True )

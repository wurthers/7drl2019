from clubsandwich.tilemap import TileMap, Cell, CellOutOfBoundsError
from clubsandwich.geom import Size, Point, Rect
from clubsandwich.director import DirectorLoop, Scene
from clubsandwich.blt.context import BearLibTerminalContext as Context
from bearlibterminal import terminal as bearlib
from enum import Enum
from window import Window
import random

window = Window()


class Terrain(Enum):
    FLOOR = 0x002E          #  .
    EMPTY = 0x0020          #  SP
    STAIRS_DOWN = 0x003E    #  >
    STAIRS_UP = 0x003C      #  <
    RAFTER = 0x003D         #  =


class Orientation(Enum):
    HORIZONTAL = 0x2500     #  ─
    VERTICAL = 0x2502       #  │
    TOP_LEFT = 0x250C       #  ┌
    TOP_RIGHT = 0x2510      #  ┐
    BOTTOM_LEFT = 0x2514    #  └
    BOTTOM_RIGHT = 0x2518   #  ┘


class Color(Enum):
    GREEN = 0xFF557e3e
    PURPLE = 0xFF847788
    ORANGE = 0xFFcf8b26
    BLUE = 0xFF525d84
    TAN = 0xFF99886c
    TEAL = 0xFF528480
    MAGENTA = 0xFF845264
    SILVER = 0xFFa1a1a1
    BROWN = 0xFF68451d
    VIPER = 0xFF1d681f
    RED = 0xFF52282b
    GREY = 0xFF42525c


class Tile(Cell):
    def __init__(self, point, color=None):
        super().__init__(point)
        self.glyph = None
        self.block = False
        self.block_sight = False
        self.color = color if color else window.fg_color
        self.open = True
        self.occupied = False
        self.occupied_by = []

    def draw_tile(self, context):
        context.color(self.color)
        context.put(Point(self.point.x, self.point.y), self.glyph)
        context.color(window.fg_color)


class Empty(Tile):
    def __init__(self, point):
        super().__init__(point)
        self.open = False
        self.terrain = Terrain.EMPTY
        self.glyph = self.terrain.value
        self.block = True


class Floor(Tile):
    def __init__(self, point, color=None):
        super().__init__(point, color=color)
        self.terrain = Terrain.FLOOR
        self.glyph = self.terrain.value


class Stairs(Tile):
    def __init__(self, point, direction, connecting_floor, dest, color=None):
        super().__init__(point, color=color)
        self.open = False
        self.direction = direction
        if self.direction == 'up':
            self.terrain = Terrain.STAIRS_UP
        elif self.direction == 'down':
            self.terrain = Terrain.STAIRS_DOWN
        self.glyph = self.terrain.value
        self.connection = connecting_floor
        self.dest = dest


class Rafter(Tile):
    def __init__(self, point, color=None):
        super().__init__(point, color=color)
        self.terrain = Terrain.RAFTER
        self.glyph = self.terrain.value


class Wall(Tile):
    def __init__(self, point, orientation, color=Color.TAN.value):
        super().__init__(point, color=color)
        self.open = False
        self.terrain = orientation
        self.glyph = self.terrain.value
        self.block = True
        self.block_sight = True

class Map:
    home = None
    attic = None
    upstairs = None
    ground = None
    basement = None
    current_floor = None

    floor_size = Size(50, 50)
    origin = Point(1, 4)
    center = Point(5, 5)
    size = Size(11, 11)

    @staticmethod
    def place_stairs(floor):
        x = random.randrange(floor.floor_size.width)
        y = random.randrange(floor.floor_size.height)
        while floor.cell(Point(x, y)).block:
            x = random.randrange(floor.floor_size.width)
            y = random.randrange(floor.floor_size.height)
        here = Point(x, y)

        to_floor = floor.points_of_interest['down']
        dest_x = random.randrange(to_floor.floor_size.width)
        dest_y = random.randrange(to_floor.floor_size.height)
        # Re-generate if we collide with a tile that is not a Floor or occupied
        while not isinstance(to_floor[Point(dest_x, dest_y)], Floor) or to_floor[Point(dest_x, dest_y)].occupied:
            dest_x = random.randrange(to_floor.floor_size.width)
            dest_y = random.randrange(to_floor.floor_size.height)
        dest = Point(dest_x, dest_y)

        floor.set_cell(here, Stairs(here, 'down', to_floor, dest))
        floor.stairs_down = floor.cell(here)
        to_floor.set_cell(dest, Stairs(dest, 'up', floor, here))
        to_floor.stairs_up = to_floor.cell(dest)

    @staticmethod
    def get_current_floor():
        return Map.current_floor.name.title()


    class FloorPlan(TileMap):
        def __init__(self, width, height, variance=0, name=None):
            super().__init__(Map.floor_size, cell_class=Empty)
            self.floor_size = Size(width, height)
            self.variance = variance
            self.name = name
            self.carve_floor()
            self.stairs_down = None
            self.stairs_up = None

        def set_cell(self, point, tile):
            try:
                self._cells[point.x][point.y] = tile
            except IndexError:
                print("Setting cell out of bounds!")
                return False

        def carve_floor(self):
            if self.variance != 0:
                width  = random.randrange(self.floor_size.width - self.variance,
                                          self.floor_size.width + self.variance)
                height = random.randrange(self.floor_size.height - self.variance,
                                          self.floor_size.width + self.variance)
            else:
                width = self.floor_size.width
                height = self.floor_size.height
            self.bounds = Rect(Point(0, 0), Size(width + 1, height + 1))
            self.area = self.bounds.with_inset(1)

            for p in self.area.points:
                self.set_cell(p, Floor(p))

            self.set_cell(self.bounds.origin, Wall(self.bounds.origin, Orientation.TOP_LEFT))
            self.set_cell(self.bounds.point_top_right, Wall(self.bounds.point_top_right, Orientation.TOP_RIGHT))
            self.set_cell(self.bounds.point_bottom_right, Wall(self.bounds.point_bottom_right,
                                                               Orientation.BOTTOM_RIGHT))
            self.set_cell(self.bounds.point_bottom_left, Wall(self.bounds.point_bottom_left, Orientation.BOTTOM_LEFT))
            for p in self.bounds.points_top:
                self.set_cell(p, Wall(p, Orientation.HORIZONTAL))
            for p in self.bounds.points_bottom:
                self.set_cell(p, Wall(p, Orientation.HORIZONTAL))
            for p in self.bounds.points_left:
                self.set_cell(p, Wall(p, Orientation.VERTICAL))
            for p in self.bounds.points_right:
                self.set_cell(p, Wall(p, Orientation.VERTICAL))


    class Home(FloorPlan):
        def __init__(self, width=6, height=6):
            super().__init__(width, height, name='home')


    class Attic(FloorPlan):
        def __init__(self, width=15, height=12, variance=1):
            super().__init__(width, height, variance=variance, name='attic')
            self.enemies = [Rodent, Silverfish, Earwig]

        def generate_tiles(self):
            for p in self.area.points:
                if p != self.stairs_up.point and p != self.stairs_up.point:
                    self.set_cell(p, Empty(p))

            path = self.stairs_up.point.points_bresenham_to(self.stairs_down.point)
            for p in path:
                if p != self.stairs_up.point and p != self.stairs_down.point:
                    self.set_cell(p, Rafter(p))

            to_fill = int(.7 * self.area.area)
            filled = 0

            while filled < to_fill:
                p = self.area.get_random_point()
                if isinstance(self.cell(p), Empty):
                    self.set_cell(p, Rafter(p))
                    filled = filled + 1


    class SecondFloor(FloorPlan):
        def __init__(self, width=20, height=20, variance=3):
            super().__init__(width, height, variance=variance, name='second story')


    class GroundFloor(FloorPlan):
        def __init__(self, width=30, height=24, variance=4):
            super().__init__(width, height, variance=variance, name='ground floor')


    class Basement(FloorPlan):
        def __init__(self, width=36, height=28, variance=5):
            super().__init__(width, height, variance=variance, name='basement')


    @staticmethod
    def generate_world():
        Map.home = Map.Home()
        Map.attic = Map.Attic()
        Map.upstairs = Map.SecondFloor()
        Map.ground = Map.GroundFloor()
        Map.basement = Map.Basement()

        Map.home = Map.Home()
        Map.home.points_of_interest = {'down': Map.attic}
        Map.place_stairs(Map.home)

        Map.attic.points_of_interest = {'down': Map.upstairs}
        Map.place_stairs(Map.attic)

        Map.upstairs.points_of_interest = {'down': Map.ground}
        Map.place_stairs(Map.upstairs)

        Map.ground.points_of_interest = {'down': Map.basement}
        Map.place_stairs(Map.ground)
        Map.current_floor = Map.home

        Map.attic.generate_tiles()


class ObjSize(Enum):
    SMALL = 1
    MEDIUM = 2
    LARGE = 3


class Entity:
    def __init__(self, x, y, on_floor, entity_type, size=ObjSize.SMALL, blocking=True, color=None, name=None):
        self.x = x
        self.y = y
        self.size = size
        self.floor = on_floor
        self.type = entity_type
        self.name = entity_type.name.lower() if name is None else name
        self.color = color if color else window.fg_color
        self.blocking = blocking
        self.tile = self.floor[Point(self.x, self.y)]
        if self.size == ObjSize.SMALL:
            self.glyph = self.type.value
        elif self.size == ObjSize.MEDIUM:
            self.glyph = self.type.value + 0xE000
        elif self.size == ObjSize.LARGE:
            self.glyph = self.type.value + 0xF000
        self.state = None
        self.occupying = None
        self.log = None
        self.handle_size()

    def get_glyph(self):
        return chr(self.type.value)

    def handle_size(self):
        if self.size == ObjSize.SMALL:
            self.occupying = [Point(self.x, self.y)]
        elif self.size == ObjSize.MEDIUM:
            self.occupying = [Point(self.x, self.y),
                              Point(self.x+1, self.y),
                              Point(self.x+1, self.y+1),
                              Point(self.x, self.y+1)]
        elif self.size == ObjSize.LARGE:
            self.occupying = [Point(self.x, self.y), Point(self.x+1, self.y+1)]
            self.occupying.extend([p for p in Point(self.x+1, self.y+1).neighbors])
            self.occupying.extend([p for p in Point(self.x+1, self.y+1).diagonal_neighbors])
        self.update_block()

    def coords(self):
        return Point(self.x, self.y)

    def wakeup(self, context):
        if self.floor is Map.current_floor:
            context.color(self.color)
            context.layer(1)
            context.put(Point(self.x, self.y), self.glyph)
            context.layer(0)
            context.color(window.fg_color)

    def update_block(self):
        for o in self.occupying:
            if self.blocking:
                self.floor[o].block = True
                if not isinstance(self, Actor):
                    self.floor[o].block_sight = True
            self.floor[o].occupied = True
            self.floor[o].occupied_by.append(self)

    def unblock(self):
        for o in self.occupying:
            if self.blocking:
                self.floor[o].block = False
            if self in self.floor[o].occupied_by and len(self.floor[o].occupied_by) == 1:
                self.floor[o].occupied = False
            self.floor[o].occupied_by.remove(self)

    def move_to(self, point):
        self.unblock()
        for o in self.occupying:
            bearlib.clear(o.x, o.y, 1, 1)
        self.x = point.x
        self.y = point.y
        self.tile = self.floor[point]
        self.handle_size()

    def draw_sight_line(self, target, context):
        position = Point(self.x, self.y)
        sight_line = position.points_bresenham_to(Point(target.x, target.y))
        try:
            context.bkcolor(Color.PURPLE.value)
            for p in sight_line:
                tile = Map.current_floor.cell(p)
                if not tile.block_sight and p != self.coords():
                    tile.draw_tile(context)
            context.bkcolor(window.bg_color)
        except CellOutOfBoundsError:
            pass


class ActorType(Enum):
    PLAYER = 0x0040     # @
    RODENT = 0x0072     # r
    SILVERFISH = 0x0073 # s
    BEETLE = 0x0042     # B
    VIPER = 0x0056      # V
    COCKROACH = 0x0063  # c
    EARWIG = 0x0049     # I
    SPIDER = 0x0042     # A
    CAT = 0x0046        # F


class ActorState(Enum):
    ALIVE = 'alive'
    DEAD = 'dead'


class EnemyMode(Enum):
    WANDER = 'wander'
    ATTACK = 'attack'
    STUNNED = 'stunned'

class Actor(Entity):
    def __init__(self, x, y, on_floor, actor_type, size=ObjSize.SMALL, blocking=True, color=None, scene=None):
        self.ammunition = 3
        self.health = 1
        self.range = 2
        self.ranged = False
        self.scene = scene
        self.state = ActorState.ALIVE
        self.mode = EnemyMode.WANDER
        super().__init__(x, y, on_floor, actor_type, size=size, blocking=blocking, color=color)

    def get_health(self):
        return self.health

    def get_ammunition(self):
        return self.ammunition

    def actor_move(self, dx, dy):
        x_dest = self.x + dx
        y_dest = self.y + dy
        try:
            target_point = Point(x_dest, y_dest)
            target_area = [Point(o.x + dx, o.y + dy) for o in self.occupying]
            dest_cell = Map.current_floor.cell(target_point)
            if dest_cell.occupied:
                for e in dest_cell.occupied_by:
                    if isinstance(self, Player) and isinstance(e, Actor) and not isinstance(e, Player):
                        self.melee_attack(e)
                        return
                    if not isinstance(self, Player) and isinstance(e, Player) and not isinstance(e, Actor):
                        self.melee_attack(e)
                        return
            for point in target_area:
                if self.floor[point].block and point not in self.occupying:
                    return
        except CellOutOfBoundsError:
            return
        else:
            self.move_to(target_point)

    def change_floors(self):
        self.unblock()
        bearlib.clear(self.x, self.y, self.size.value, self.size.value)
        if self.type is ActorType.PLAYER:
            Map.current_floor = self.tile.connection
        self.floor = self.tile.connection
        self.x = self.tile.dest.x
        self.y = self.tile.dest.y
        self.tile = self.floor[self.tile.dest]
        self.handle_size()

    def suffer(self):
        self.health = self.health - 1
        if self.health <= 0:
            self.state = ActorState.DEAD
            self.scene.log_msg("The {} dies!".format(self.name))
            self.unblock()

    def ai_behavior(self, player):
        if random.randrange(0, 10) == 0 or self.mode == EnemyMode.STUNNED:
            return

        target_range = Rect(Point(self.x - self.range, self.y - self.range),
                            Size(self.range * 2 + 1, self.range * 2 + 1))
        pos = Point(self.x, self.y)
        player_pos = Point(player.x, player.y)
        neighbors = [p for p in pos.neighbors]
        neighbors.extend([p for p in pos.diagonal_neighbors])
        if target_range.contains(player_pos):
            self.mode = EnemyMode.ATTACK
            if self.ranged:
                self.ranged_attack(self.floor.cell(player_pos))
        else:
            self.mode = EnemyMode.WANDER
        if self.mode == EnemyMode.WANDER:
            dest = random.choice(neighbors)
            dx = dest.x - pos.x
            dy = dest.x - pos.x
            self.actor_move(dx, dy)
        if self.mode == EnemyMode.ATTACK:
            if player_pos in neighbors:
                self.melee_attack(player)
            else:
                better_path = pos.points_bresenham_to(player_pos)
                worse_path = pos.path_L_to(player_pos)
                for p in better_path:
                    if p in neighbors and not self.floor.cell(p).block:
                        dx = p.x - pos.x
                        dy = p.x - pos.x
                        self.actor_move(dx, dy)
                        return
                for p in worse_path:
                    if p in neighbors and not self.floor.cell(p).block:
                        dx = p.x - pos.x
                        dy = p.x - pos.x
                        self.actor_move(dx, dy)
                        return

    def melee_attack(self, target):
        if isinstance(target, Sibling) and isinstance(self, Player):
            target.bestow(self)
            return
        elif isinstance(self, Player):
            self.scene.log_msg("You attack the {} with your sword.".format(target.name))
        else:
            self.scene.log_msg("The {} attacks {}.".format(self.name.title(),
                                                           'you' if isinstance(target, Player) else target.name))
        target.suffer()

    def ranged_attack(self, target_cell):
        target = None
        if self.ammunition <= 0:
            if isinstance(self, Player):
                self.scene.log_msg("All out of ammo!")
            return
        self.ammunition = self.ammunition - 1
        if target_cell.occupied:
            for e in target_cell.occupied_by:
                target = e
                if isinstance(self, Player) and isinstance(target, Actor) and not isinstance(target, Player):
                    if target is not None:
                        self.scene.log_msg("You sling a pushpin at the {}!".format(self.name.title(), target.name))
                        target.suffer()
                        return
                elif isinstance(self, Actor) and not self.type == ActorType.PLAYER and isinstance(e, Player):
                        self.scene.log_msg("The {} spits venom at you.".format(self.name.title()))
                        target.suffer()
                        return

    def on_death(self):
        pass


class Player(Actor):
    def __init__(self, x, y, on_floor, color=None, scene=None):
        super().__init__(x, y, on_floor, ActorType.PLAYER, size=ObjSize.SMALL, blocking=True, color=color, scene=scene)
        self.ammunition = 0
        self.health = 15
        self.range = 3

    def position(self):
        return Point(Map.origin.x - self.x, Map.origin.y - self.y)


class Sibling(Actor):
    def __init__(self, x, y, on_floor, color=None, scene=None):
        super().__init__(x, y, on_floor, ActorType.PLAYER, size=ObjSize.MEDIUM, blocking=True, color=color,
                         scene=scene)
        self.name = 'big sibling'
        self.has_ammo = True

    def bestow(self, player):
        if self.has_ammo:
            self.scene.log_msg('Take this ammo for your sling.') # Add dialogue
            player.ammunition = player.ammunition + 20
            self.has_ammo = False
        self.scene.log_msg('Travel safe little sibling, and beware the {}'\
                           ' in the basement!'.format(self.scene.boss.name.title()))


class Rodent(Actor):
    def __init__(self, x, y, on_floor, color=None, scene=None):
        super().__init__(x, y, on_floor, ActorType.RODENT, size=ObjSize.SMALL, blocking=True, color=color, scene=scene)


class Silverfish(Actor):
    def __init__(self, x, y, on_floor, color=Color.SILVER.value, scene=None):
        super().__init__(x, y, on_floor, ActorType.SILVERFISH, size=ObjSize.SMALL, blocking=True, color=color,
                         scene=scene)


class Cockroach(Actor):
    def __init__(self, x, y, on_floor, color=Color.BROWN.value, scene=None):
        super().__init__(x, y, on_floor, ActorType.COCKROACH, size=ObjSize.SMALL, blocking=True, color=color,
                         scene=scene)
        self.health = 2


class Earwig(Actor):
    def __init__(self, x, y, on_floor, color=Color.TAN.value, scene=None):
        super().__init__(x, y, on_floor, ActorType.EARWIG, size=ObjSize.SMALL, blocking=True, color=color,
                         scene=scene)
        self.ammo = 2
        self.range = 3
        self.ranged = True


class Spider(Actor):
    def __init__(self, x, y, on_floor, color=Color.RED.value, scene=None):
        super().__init__(x, y, on_floor, ActorType.SPIDER, size=ObjSize.SMALL, blocking=True, color=color,
                         scene=scene)
        self.ammo = 4
        self.range = 3
        self.health = 2
        self.ranged = True


class Cat(Actor):
    def __init__(self, x, y, on_floor, color=Color.ORANGE.value, scene=None):
        super().__init__(x, y, on_floor, ActorType.CAT, size=ObjSize.MEDIUM, blocking=True, color=color,
                         scene=scene)
        self.stun_count = 3
        self.range = 8

    def ai_behavior(self, player):
        if self.mode == EnemyMode.STUNNED:
            self.stun_count = self.stun_count - 1
            if self.stun_count == 0:
                self.mode == EnemyMode.WANDER

        super().ai_behavior(player)

    def suffer(self):
        self.state = ActorState.STUNNED
        self.stun_count = 3
        self.scene.log_msg('You knock the {} out!'.format(self.name))


class Viper(Actor):
    def __init__(self, x, y, on_floor, color=Color.VIPER.value, scene=None):
        super().__init__(x, y, on_floor, ActorType.VIPER, size=ObjSize.MEDIUM, blocking=True, color=color,
                         scene=scene)
        self.health = 4
        self.ammo = 3
        self.range = 4
        self.ranged = True

    def on_death(self):
        self.scene.push_scene(VictoryScene())


class Beetle(Actor):
    def __init__(self, x, y, on_floor, color=Color.BROWN.value, scene=None):
        super().__init__(x, y, on_floor, ActorType.BEETLE, size=ObjSize.MEDIUM, blocking=True, color=color,
                         scene=scene)
        self.name = 'Rhino Beetle'
        self.health = 5
        self.range = 2

    def on_death(self):
        self.scene.push_scene(VictoryScene())


class ItemType(Enum):
    BED = 0x005F    #  _
    DESK = 0x0023   #  #
    CHAIR = 0x007E #  ~
    PIN = 0x002F    #  /
    VIAL = 0x0021   #  !


class Item(Entity):
    def __init__(self, x, y, on_floor, item_type, size=ObjSize.SMALL, blocking=False, usable=False, color=None):
        self.usable = usable
        super().__init__(x, y, on_floor, item_type, size=size, blocking=blocking, color=color)

    def on_use(self, player):
        pass


class Bed(Item):
    def __init__(self, x, y, on_floor, size=ObjSize.LARGE, color=Color.BLUE.value):
        self.name = 'Bed'
        blocking = False
        if size.value < ObjSize.LARGE.value:
            blocking = True
        super().__init__(x, y, on_floor, ItemType.BED, size=size, blocking=blocking, color=color)


class Desk(Item):
    def __init__(self, x, y, on_floor, size=ObjSize.LARGE, color=None):
        self.name = 'Desk'
        super().__init__(x, y, on_floor, ItemType.DESK, size=size, blocking=True, color=color)


class Chair(Item):
    def __init__(self, x, y, on_floor, size=ObjSize.MEDIUM, color=None):
        self.name = 'Chair'
        super().__init__(x, y, on_floor, ItemType.CHAIR, size=size, blocking=False, color=color)


class Vial(Item):
    def __init__(self, x, y, on_floor, size=ObjSize.SMALL, blocking=False, usable=True, color=None):
        self.name = 'Vial'
        super().__init__(x, y, on_floor, ItemType.PIN, size=size, blocking=blocking, usable=usable, color=color)

    def on_use(self, player):
        player.health = player.health + 2


class Pin(Item):
    def __init__(self, x, y, on_floor, size=ObjSize.SMALL, color=None):
        self.name = 'Pin'
        super().__init__(x, y, on_floor, ItemType.PIN, size=size, blocking=False, usable=True, color=color)

    def on_use(self, player):
        player.ammunition = player.ammunition + 1


class Direction(Enum):
    NE = bearlib.TK_KP_9
    NW = bearlib.TK_KP_7
    SE = bearlib.TK_KP_3
    SW = bearlib.TK_KP_1
    N = bearlib.TK_KP_8
    S = bearlib.TK_KP_2
    W = bearlib.TK_KP_4
    E = bearlib.TK_KP_6
    WAIT = bearlib.TK_KP_5

    VIM_NE = bearlib.TK_U
    VIM_NW = bearlib.TK_Y
    VIM_SE = bearlib.TK_N
    VIM_SW = bearlib.TK_B
    VIM_N = bearlib.TK_K
    VIM_S = bearlib.TK_J
    VIM_W = bearlib.TK_H
    VIM_E = bearlib.TK_L
    VIM_WAIT = bearlib.TK_SPACE


class Command(Enum):
    UP_STAIRS = bearlib.TK_COMMA
    DOWN_STAIRS = bearlib.TK_PERIOD
    FIRE = bearlib.TK_F
    PICKUP = bearlib.TK_COMMA


class Input:
    def __init__(self, player, entities, context, scene):
        self.player = player
        self.entities = entities
        self.context = context
        self.scene = scene

    def handle_key(self, key):
        try:
            direction = Direction(key)
            self.handle_move(direction)
            return
        except ValueError:
            pass

        try:
            command = Command(key)
            self.handle_command(command)
        except ValueError:
            pass

    def handle_move(self, direction):
        dx = 0
        dy = 0
        if direction == Direction.N or direction == Direction.VIM_N:
            dy = -1
        elif direction == Direction.S or direction == Direction.VIM_S:
            dy = 1
        elif direction == Direction.W or direction == Direction.VIM_W:
            dx = -1
        elif direction == Direction.E or direction == Direction.VIM_E:
            dx = 1
        elif direction == Direction.NE or direction == Direction.VIM_NE:
            dx = 1
            dy = -1
        elif direction == Direction.NW or direction == Direction.VIM_NW:
            dx = -1
            dy = -1
        elif direction == Direction.SE or direction == Direction.VIM_SE:
            dx = 1
            dy = 1
        elif direction == Direction.SW or direction == Direction.VIM_SW:
            dx = -1
            dy = 1
        elif direction == Direction.W or direction == Direction.VIM_WAIT:
            return
        self.player.actor_move(dx, dy)

    @staticmethod
    def handle_cursor(direction):
        dx = 0
        dy = 0
        if direction == Direction.N or direction == Direction.VIM_N:
            dy = -1
        elif direction == Direction.S or direction == Direction.VIM_S:
            dy = 1
        elif direction == Direction.W or direction == Direction.VIM_W:
            dx = -1
        elif direction == Direction.E or direction == Direction.VIM_E:
            dx = 1
        elif direction == Direction.NE or direction == Direction.VIM_NE:
            dx = 1
            dy = -1
        elif direction == Direction.NW or direction == Direction.VIM_NW:
            dx = -1
            dy = -1
        elif direction == Direction.SE or direction == Direction.VIM_SE:
            dx = 1
            dy = 1
        elif direction == Direction.SW or direction == Direction.VIM_SW:
            dx = -1
            dy = 1
        return Point(dx, dy)

    def handle_command(self, command):
        if command == Command.UP_STAIRS and bearlib.state(bearlib.TK_SHIFT):
            if isinstance(self.player.tile, Stairs) and self.player.tile.direction == 'up':
                self.player.change_floors()
        elif command == Command.DOWN_STAIRS and bearlib.state(bearlib.TK_SHIFT):
            if isinstance(self.player.tile, Stairs) and self.player.tile.direction == 'down':
                self.player.change_floors()
        elif command == Command.PICKUP:
            for e in self.player.tile.occupied_by:
                if isinstance(e, Item) and e.usable:
                    e.on_use(self.player)
                    e.unblock()
                    self.entities.remove(e)
        elif command == Command.FIRE:
            cursor = Point(self.player.x, self.player.y)
            key = bearlib.read()
            sight_line = [cursor]
            while key != bearlib.TK_F:
                if key == bearlib.TK_ESCAPE:
                    return
                try:
                    direction = Direction(key)
                except ValueError:
                    continue
                cursor_delta = self.handle_cursor(direction)
                try:
                    self.player.floor.cell(cursor + cursor_delta)
                except CellOutOfBoundsError:
                    key = bearlib.read()
                    continue
                player_pos = Point(self.player.x, self.player.y)
                target_range = Rect(Point(self.player.x - self.player.range, self.player.y - self.player.range),
                                    Size(self.player.range * 2 + 1, self.player.range * 2 + 1))
                if not target_range.contains(cursor + cursor_delta):
                    key = bearlib.read()
                    continue
                cursor = cursor + cursor_delta
                for p in sight_line:
                    self.context.clear_area(Rect(Point(p.x, p.y), Size(1, 1)))
                    self.player.floor.cell(p).draw_tile(self.context)
                sight_line = [p for p in player_pos.points_bresenham_to(cursor)]
                self.context.layer(0)
                for p in sight_line:
                    self.context.clear_area(Rect(Point(p.x, p.y), Size(1, 1)))
                self.player.draw_sight_line(cursor, self.context)
                self.context.refresh()
                key = bearlib.read()
            for p in sight_line:
                if self.player.floor.cell(p).block_sight:
                    for p in sight_line:
                        self.context.clear_area(Rect(Point(p.x, p.y), Size(1, 1)))
                    return
            target_cell = self.player.floor.cell(cursor)
            self.player.ranged_attack(target_cell)




def pprint_center(strings):
    """
    Prints a string or list of strings in the center of the screen.
    :param strings: String or list of strings to be printed
    :type strings: str | str[]
    """
    height = window.height
    width = window.width
    cellsize, _  = window.cell_size.split('x')
    cell_width = int(cellsize)
    center = int(width/2)

    bearlib.clear()
    bearlib.layer(1)
    bearlib.composition("TK_ON")
    if not isinstance(strings, list):
        strings = [strings]
    y = int(height/2 - len(strings)/2)
    for i, s in enumerate(strings):
        middle_char = int(len(s)/2)
        x = int(center - middle_char)
        pos = 0
        for c in s:
            offset = (center - x) * (cell_width / 2)
            bearlib.put_ext(x, y + i, int(offset), 0, c)
            x = x + 1
            pos = pos + 1
    bearlib.composition("TK_OFF")
    bearlib.layer(0)
    bearlib.refresh()


class SceneLoop(DirectorLoop):
    def get_initial_scene(self):
        return StartScene()


class StartScene(Scene):
    def terminal_update(self, is_active=False):
        pprint_center(["little sibling", "A 7drl by wurthers", "", "Space - Start", "Esc, Q - Quit"])

    def terminal_read(self, val):
        if val == bearlib.TK_SPACE:
            self.director.push_scene(GameScene())
        elif val == bearlib.TK_ESCAPE or val == bearlib.TK_Q:
            self.director.pop_scene()


class GameScene(Scene):
    def __init__(self):
        self.entities = []
        self.log = []
        self.log_height = 4
        self.gutter = []
        self.context = Context()

        Map.generate_world()
        self.populate()
        self.input = Input(self.player, self.entities, self.context, self)
        self.relative_pos = self.player.position() + Map.center

        self.gutter.extend([{'Health': self.player.get_health}, {'Ammo': self.player.get_ammunition}])
        super().__init__()

    def pprint(x, y, string):
        """
        Pretty-prints a string starting at (x,y).
        :param x: x coordinate
        :type x: int
        :param y: y coordinate
        :type y: int
        """
        bearlib.layer(1)
        bearlib.composition("TK_ON")
        pos = x
        cell_size, _  = window.cell_size.split('x')
        cell_width = int(cell_size)
        for c in string:
            if pos >= window.width - x - 1:
                pos = pos + 1
                offset = (pos - x) * (cell_width / 2)
                bearlib.put_ext(x, y, int(offset), 0, c)
            else:
                pos = pos + 1
                offset = 0 - pos * (cell_width / 2)
                bearlib.put_ext(pos, y, int(offset), 0, c)
        bearlib.layer(0)
        bearlib.composition("TK_OFF")

    @staticmethod
    def find_open_point(floor, size):
        while True:
            p = Map.home.area.get_random_point()
            if size == ObjSize.SMALL:
                area = [Point(p.x, p.y)]
            elif size == ObjSize.MEDIUM:
                area = [Point(p.x, p.y),
                        Point(p.x+1, p.y),
                        Point(p.x+1, p.y+1),
                        Point(p.x, p.y+1)]
            elif size == ObjSize.LARGE:
                area = [Point(p.x+1, p.y+1)]
                area.extend([a for a in Point(p.x+1, p.y+1).neighbors])
                area.extend([a for a in Point(p.x+1, p.y+1).diagonal_neighbors])

            found = True
            try:
                for a in area:
                    if floor.cell(a).block or not floor.cell(a).open:
                        found = False
                if found:
                    return p
            except CellOutOfBoundsError:
                pass
            continue

    def populate(self):

        items = [Vial, Pin]

        floor = Map.home
        p = GameScene.find_open_point(floor, ObjSize.SMALL)
        your_bed = Bed(p.x, p.y, floor, size=ObjSize.SMALL, color=Color.GREEN.value)
        self.player = Player(p.x, p.y, floor, color=Color.GREEN.value, scene=self)

        p = GameScene.find_open_point(floor, ObjSize.MEDIUM)
        sib_bed = Bed(p.x, p.y, floor, size=ObjSize.MEDIUM, color=Color.BLUE.value)
        self.sibling = Sibling(p.x, p.y, floor, color=Color.BLUE.value, scene=self)

        p = GameScene.find_open_point(floor, ObjSize.SMALL)
        for c in p.neighbors:
            if not floor.cell(c).block and not floor.cell(c).occupied:
                chair = Chair(c.x, c.y, floor, size=ObjSize.SMALL, color=Color.GREEN.value)
                self.entities.append(chair)
                break
        desk = Desk(p.x, p.y, floor, size=ObjSize.SMALL, color=Color.GREEN.value)
        self.entities.extend([self.sibling, your_bed, sib_bed, desk])


        floor = Map.attic
        enemies = [Rodent, Silverfish, Earwig]
        for n in range (0, random.randrange(4, 8)):
            enemy = random.choice(enemies)
            p = GameScene.find_open_point(floor, ObjSize.SMALL)
            self.entities.append(enemy(p.x, p.y, floor, scene=self))


        floor = Map.upstairs
        enemies = [Rodent, Cockroach, Silverfish]

        p = GameScene.find_open_point(floor, ObjSize.LARGE)
        bed = Bed(p.x, p.y, floor, size=ObjSize.LARGE, color=Color.GREY.value)
        self.entities.append(bed)

        p = GameScene.find_open_point(floor, ObjSize.LARGE)
        desk = Desk(p.x, p.y, floor, size=ObjSize.LARGE, color=Color.GREY.value)
        self.entities.append(desk)


        for n in range (0, random.randrange(5, 10)):
            enemy = random.choice(enemies)
            p = GameScene.find_open_point(floor, ObjSize.SMALL)
            self.entities.append(enemy(p.x, p.y, floor, scene=self))
        if random.randrange(0, 10) < 3:
            p = GameScene.find_open_point(floor, ObjSize.MEDIUM)
            cat = Cat(p.x, p.y, floor, scene=self)
            self.entities.append(cat)
        for n in range (0, random.randrange(1, 3)):
            item = random.choice(items)
            p = GameScene.find_open_point(floor, ObjSize.SMALL)
            self.entities.append(item(p.x, p.y, floor))


        floor = Map.ground
        p = GameScene.find_open_point(floor, ObjSize.MEDIUM)
        chair = Chair(p.x, p.y, floor, size=ObjSize.MEDIUM, color=Color.GREY.value)
        self.entities.append(chair)

        p = GameScene.find_open_point(floor, ObjSize.MEDIUM)
        chair = Chair(p.x, p.y, floor, size=ObjSize.MEDIUM, color=Color.GREY.value)
        self.entities.append(chair)

        enemies = [Spider, Cockroach]
        for n in range (0, random.randrange(7, 12)):
            enemy = random.choice(enemies)
            p = GameScene.find_open_point(floor, ObjSize.SMALL)
            self.entities.append(enemy(p.x, p.y, floor, scene=self))
        if random.randrange(0, 10) < 5:
            p = GameScene.find_open_point(floor, ObjSize.MEDIUM)
            cat = Cat(p.x, p.y, floor, scene=self)
            self.entities.append(cat)
        for n in range (0, random.randrange(3, 5)):
            item = random.choice(items)
            p = GameScene.find_open_point(floor, ObjSize.SMALL)
            self.entities.append(item(p.x, p.y, floor))


        floor = Map.basement
        enemies = [Cockroach, Spider, Earwig, Rodent, Silverfish]
        for n in range (0, random.randrange(12, 15)):
            enemy = random.choice(enemies)
            p = GameScene.find_open_point(floor, ObjSize.SMALL)
            self.entities.append(enemy(p.x, p.y, floor, scene=self))
        if random.randrange(0, 10) < 5:
            p = GameScene.find_open_point(floor, ObjSize.MEDIUM)
            cat = Cat(p.x, p.y, floor, scene=self)
            self.entities.append(cat)
        for n in range (0, random.randrange(4, 6)):
            item = random.choice(items)
            p = GameScene.find_open_point(floor, ObjSize.SMALL)
            self.entities.append(item(p.x, p.y, floor))


        p = GameScene.find_open_point(floor, ObjSize.SMALL)
        bosses = [Beetle, Viper]
        boss = random.choice(bosses)
        self.boss = boss(p.x, p.y, floor, scene=self)
        self.entities.append(self.boss)

        # Generate entities one floor at a time.
        # Needs to take place after house generation
        # Check whether an area is occupied before placing an entity.
        # Define a min and max number of creatures to generate for each floor.
        # Pick which actors to generate from an array of actors that belong to that floor.
        # For the basement, handle generating a boss or bosses.
        # Handle furniture in the same way as actors.

    def log_msg(self, msg):
        cutoff = window.width - 3
        while len(msg) > cutoff*2:
            sub_msg, trailing = msg[:cutoff*2].rsplit(' ', 1)
            self.log.append(sub_msg)
            msg = trailing + msg[cutoff*2:]
        self.log.append(msg)

    def print_log(self):
        for i, msg in enumerate(self.log[-self.log_height:]):
            GameScene.pprint(0, i, msg)

    def draw_gutter(self):
        GameScene.pprint(Map.size.width + 2, self.log_height + 1, '{}'.format(Map.get_current_floor()))
        del self.gutter[2:]
        edict = {}
        for e in self.entities:
            if self.bounds.contains(Point(e.x, e.y) + self.relative_pos) and e.floor == self.player.floor:
                if e.name.title() not in edict:
                    item = {e.name.title(): e.get_glyph}
                    edict.update(item)
                    self.gutter.append(item)

        for i, d in enumerate(self.gutter):
            for k, v in d.items():
                val = v()
                sp = ' ' * (12 - len(k))
                GameScene.pprint(Map.size.width + 2, i + self.log_height + 2,
                                 '{}{}{}'.format(k, sp, str(val)))

    def draw_hints(self):
        GameScene.pprint(0, window.height - 1,  "f  Ranged  |  ,  Pickup  |  < > Stairs")

    def terminal_update(self, is_active=False):
        if self.player.state == ActorState.DEAD:
            self.director.replace_scene(DeathScene())
        bearlib.clear()
        self.relative_pos = self.player.position() + Map.center
        self.bounds = Rect(Map.origin, Map.size)
        with self.context.translate(self.relative_pos):
            for i in Map.current_floor.cells:
                if self.bounds.contains(i.point + self.relative_pos):
                    if not i.occupied:
                        i.draw_tile(self.context)
            for e in self.entities:
                if isinstance(e, Actor):
                    if e.state == ActorState.DEAD:
                        self.entities.remove(e)
                        e.on_death()
                        continue
                if e.floor == Map.current_floor:
                    if self.bounds.contains(Point(e.x, e.y) + self.relative_pos):
                        e.wakeup(self.context)
            self.player.wakeup(self.context)
            self.print_log()
            self.draw_gutter()
            self.draw_hints()
            bearlib.refresh()

    def terminal_read(self, val):
        with self.context.translate(self.relative_pos):
            for e in self.entities:
                if isinstance(e, Actor) and e.floor == Map.current_floor and e.type != ActorType.PLAYER:
                    e.ai_behavior(self.player)
            self.input.handle_key(val)
        if val == bearlib.TK_ESCAPE or val == bearlib.TK_Q:
                self.context.clear()
                pprint_center(["Are you sure you", "want to quit?", "", "Space - Yes ", "Esc - No"])
                self.context.refresh()
                while True:
                    key = bearlib.read()
                    if key == bearlib.TK_SPACE:
                        self.director.quit()
                        break
                    elif key == bearlib.TK_ESCAPE:
                        break


class DeathScene(Scene):
    def terminal_update(self, is_active=False):
        pprint_center(["You died. :(", "R - Restart", "Q, ESC - Quit"])

    def terminal_read(self, val):
        if val == bearlib.TK_R:
            self.director.replace_scene(GameScene())
        elif val == bearlib.TK_ESCAPE or val == bearlib.TK_Q:
            self.director.quit()


class VictoryScene(Scene):
    def terminal_update(self, is_active=False):
        pprint_center(["You defeated the creature", "in the basement!", "", "Your big sibling will be OK.",
                      "R - Restart", "Q, ESC - Quit"])

    def terminal_read(self, val):
        if val == bearlib.TK_R:
            self.director.replace_scene(GameScene())
        elif val == bearlib.TK_ESCAPE or val == bearlib.TK_Q:
            self.director.quit()


def main():
    SceneLoop().run()


if __name__ == '__main__':
    main()
